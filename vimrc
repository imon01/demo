set cursorline
set number




set tabstop=4 shiftwidth=4 expandtab
""autocmd FileType make set noexpandtab shiftwidth=8 softtabstop=0
set hlsearch
hi Folded ctermbg=240

highlight myerror guifg=red guibg=green
syntax match myerror /ERROR/

highlight mywarno guifg=yellow guibg=green
syntax match mywarno /WARNO/
 
highlight mytodo guifg=white guibg=green
syntax match mytodo /TODO/
