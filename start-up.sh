#! /bin/bash

# .bashrc extension for configuring environment variables 
#   and


export U_OPT=$HOME/opt


#env variable required for MSPFlasher
export LD_LIBRARY_PATH=$U_OPT/MSPFlasher_1.3.20:$LD_LIBRARY_PATH 

echo -e local env variables set:
echo -e [U_OPT]: $U_OPT
echo -e [LD_LIBRARY_PATH]:$LD_LIBRARY_PATH
echo -e [CURRENT_PATH]: $(pwd)

